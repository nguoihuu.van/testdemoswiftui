//
//  StartView.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 27/07/2022.
//

import SwiftUI

struct StartView: View {
    @AppStorage("onboarding") var isOnBoardingViewActive: Bool = true
    var body: some View {
        ZStack {
            if isOnBoardingViewActive {
                OnBoardingView()
            } else {
                HomeView()
            }
        }
    }
}

struct StartView_Previews: PreviewProvider {
    static var previews: some View {
        StartView()
    }
}
