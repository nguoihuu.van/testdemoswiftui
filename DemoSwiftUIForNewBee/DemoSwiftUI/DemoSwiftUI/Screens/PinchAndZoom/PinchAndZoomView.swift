//
//  PinchAndZoomView.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 28/07/2022.
//

import SwiftUI

struct PinchAndZoomView: View {
    //MARK: - PROPERTY
    @State private var isAnimating: Bool = false
    @State private var imageScale: CGFloat = 1
    @State private var imageOffet: CGSize = .zero
    
    //MARK: - FUNCTION
    func resetImageState() {
        return withAnimation(.spring()) {
            imageScale = 1
            imageOffet = .zero
        }
    }
    
    //MARK: - BODY
    var body: some View {
        NavigationView {
            ZStack {
                //MARK: - PAGE IMAGE
                Image("magazine-front-cover")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(10)
                    .padding()
                    .shadow(color: .black.opacity(0.2), radius: 12, x: 2, y: 2)
                    .opacity(isAnimating ? 1 : 0)
                    .offset(x: imageOffet.width, y: imageOffet.height)
                    .scaleEffect(imageScale)
                //MARK: - 1. Tap gesture
                    .onTapGesture(count: 2, perform: {
                        if imageScale == 1 {
                            withAnimation(.spring()) {
                                imageScale = 5
                            }
                        } else {
                            resetImageState()
                        }
                    })
                //MARK: - DRAG GESTURE
                    .gesture(
                        DragGesture()
                            .onChanged({ value in
                                withAnimation(.linear(duration: 1)) {
                                    imageOffet = value.translation
                                }
                            })
                            .onEnded({_ in
                                if imageScale <= 1 {
                                    resetImageState()
                                }
                            })
                    )
            }//: ZSTACK
            .navigationTitle("Pinch & Zoom")
            .navigationBarTitleDisplayMode(.inline)
            .onAppear(perform: {
                withAnimation(.linear(duration: 1)) {
                    isAnimating = true
                }
            })
        }//: NAVIGATION VIEW
        .navigationViewStyle(.stack)
        
    }
}

struct PinchAndZoomView_Previews: PreviewProvider {
    static var previews: some View {
        PinchAndZoomView()
            .preferredColorScheme(.dark)
    }
}
