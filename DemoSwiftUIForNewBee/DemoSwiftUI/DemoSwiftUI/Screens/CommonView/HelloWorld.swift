//
//  HelloWorld.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 27/07/2022.
//

import SwiftUI

struct HelloWorld: View {
    @State var isShowAlert: Bool = false
    @State var message: String = ""
    
    var body: some View {
        VStack {
            HStack {
                ExtractedView(number: 10, isShowAlert: $isShowAlert, message: $message)
                ExtractedView(number: 10, isShowAlert: $isShowAlert, message: $message)
                ExtractedView(number: 10, isShowAlert: $isShowAlert, message: $message)
            }
            CustomsView()
            .alert(isPresented: $isShowAlert) {
                Alert(title: Text("Error"), message: Text(message))
            }
        }
    }
}

struct HelloWorld_Previews: PreviewProvider {
    static var previews: some View {
        HelloWorld()
    }
}

struct CustomsView: View {
    
    var body: some View {
        Text("Hell HieuMice")
    }
}

struct ExtractedView: View {
    @State var number: Int
    @Binding var isShowAlert: Bool
    @Binding var message: String
    
    
    var body: some View {
        VStack(alignment: .center) {
            Button(action: {
                if number < 20 {
                    self.number += 1
                } else {
                    isShowAlert = true
                    message = "Maximum"
                }
            }) {
                Image(systemName: "arrowtriangle.up.fill")
                    .resizable()
                    .frame(width: 50, height: 50)
                    .foregroundColor(.red)
            }
            .frame(width: 50, height: 50)
            Text("\(number)")
                .font(.title)
                .fontWeight(.bold)
                .foregroundColor(.blue)
                .multilineTextAlignment(.center)
            Button(action: {
                self.number -= 1
            }) {
                Image(systemName: "arrowtriangle.down.fill")
                    .resizable()
                    .frame(width: 50, height: 50)
            }
            .frame(width: 50, height: 50)
        }
        .frame(width: 50, height: 200)
    }
}
