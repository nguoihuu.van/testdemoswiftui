//
//  ContentView.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 26/07/2022.
//

import SwiftUI

struct ContentView: View {
    let screenSize: CGRect = UIScreen.main.bounds
    var body: some View {
        VStack {
            Image(systemName: "cloud.heavyrain.fill")
                .resizable()
                .frame(width: 200, height: 200, alignment: .center)
                .clipShape(Circle())
                .overlay(Circle().stroke(.red, lineWidth: 4))
                .shadow(radius: 10)
                .font(.largeTitle)
                .foregroundColor(.red)
            Spacer()
            HStack {
                Image(systemName: "cloud.heavyrain.fill")
                    .resizable()
                    .frame(width: 200, height: 200, alignment: .center)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(.red, lineWidth: 4))
                    .shadow(radius: 10)
                    .font(.largeTitle)
                    .foregroundColor(.red)
                    .onTapGesture {
                        print("aaaaaaaaaa!")
                    }
                Spacer()
                Text("Hello world")
                    .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
                    .background(LinearGradient(gradient: Gradient(colors: [.green, .yellow, .red, .blue, .white]), startPoint: .top, endPoint: .trailing))
                    .foregroundColor(.white)
            }
            Button {
                debugPrint("Tap button")
            } label: {
                //Image(systemName: "cloud.heavyrain.fill")
                Text("OK")
                    .foregroundColor(.white)
            }
            .padding(.all)
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
            .background(LinearGradient(colors: [.white, .yellow, .red], startPoint: .top, endPoint: .bottom))
            .edgesIgnoringSafeArea(.bottom)
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
        ContentView().previewLayout(.fixed(width: 568, height: 320))
    }
}
