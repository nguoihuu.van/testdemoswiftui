//
//  File.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 27/07/2022.
//

import Foundation
typealias APIModel = Decodable & Identifiable
class Song: APIModel {
    var id: String
    var artistName: String
    var releaseDate: String
    var name: String
    var collectionName: String
    var kind: String
    var copyright: String
    var artistId: String
    var artistUrl: String
    var artworkUrl100: String
}

struct SongFeed: Decodable {
    var results: [Song]
}

struct SongResult: Decodable {
    var feed: SongFeed
}
