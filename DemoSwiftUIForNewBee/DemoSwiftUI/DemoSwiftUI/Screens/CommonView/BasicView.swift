//
//  BasicView.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 27/07/2022.
//

import SwiftUI

extension Image {
    func imageModifier() -> some View {
        self
            .resizable()
            .scaledToFit()
    }
    
    func iconModifier() -> some View {
        self
            .imageModifier()
            .frame(maxWidth: 128)
            .foregroundColor(.purple)
            .opacity(0.5)
    }
}

struct BasicView: View {
    private let imageUrl: String = "https://credo.academy/credo-academy@3x.png"
    var body: some View {
        //MARK: - basic
        //AsyncImage(url: URL(string: imageUrl))
        //MARK: - Scale
        //AsyncImage(url: URL(string: imageUrl), scale: 3.0)
        //MARK: - Place holder
        /*
         AsyncImage(url: URL(string: imageUrl)) { image in
            image
                .imageModifier()
        } placeholder: {
            Image(systemName: "photo.circle.fill")
                .iconModifier()
        }
        .padding(40)
         */
        
        //MARK: - Phase
        /*
         AsyncImage(url: URL(string: imageUrl)) { phase in
            //Succes: the image successfully loaded
            //Failure: image fail to load with an error
            //Empty: No image is loaded
            if let image = phase.image {
                image.imageModifier()
            } else if phase.error != nil {
                Image(systemName: "ant.circle.fill").iconModifier()
            } else {
                Image(systemName: "photo.circle.fill").iconModifier()
            }
        }
        .padding(40)
         */
        
        //MARK: - 5 Animation
        AsyncImage(url: URL(string: imageUrl), transaction: Transaction(animation: .spring(response: 0.5, dampingFraction: 0.6, blendDuration: 0.25))) { phase in
            switch phase {
            case .success(let image):
                image
                    .imageModifier()
//                    .transition(.move(edge: .bottom))
                    .transition(.slide)
            case .failure(_):
                Image(systemName: "ant.circle.fill").iconModifier()
            case .empty:
                Image(systemName: "photo.circle.fill").iconModifier()
            @unknown default:
                ProgressView()
            }
        }
        .padding(40)
    }
}

struct BasicView_Previews: PreviewProvider {
    static var previews: some View {
        BasicView()
    }
}
