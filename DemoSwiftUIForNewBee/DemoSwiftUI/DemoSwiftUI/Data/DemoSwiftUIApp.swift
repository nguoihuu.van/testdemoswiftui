//
//  DemoSwiftUIApp.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 26/07/2022.
//

import SwiftUI

@main
struct DemoSwiftUIApp: App {
    @Environment(\.scenePhase) private var scenePhase
    
    init() {
        
    }
    
    var body: some Scene {
        WindowGroup {
            //ContentView()
            //HelloWorld()
            //SongListView()
            //BasicView()
            StartView()
                .onOpenURL { url in
                    print(url.absoluteURL)
                }
        }.onChange(of: scenePhase) { phase in
            switch phase {
            case .background:
                print("Background")
            case .inactive:
                print("inactive")
            case .active:
                print("active")
            @unknown default:
                print("default")
            }
        }
    }
}
