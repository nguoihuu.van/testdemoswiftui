//
//  AudioPlayer.swift
//  DemoSwiftUI
//
//  Created by HieuMice on 28/07/2022.
//

import Foundation
import AVFoundation

var audioPlay: AVAudioPlayer?

func playSound(sound: String, type: String) {
    if let path = Bundle.main.path(forResource: sound, ofType: type) {
        do {
            audioPlay = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            audioPlay?.play()
        } catch {
            print("Could not play the sound file.")
        }
    }
}
